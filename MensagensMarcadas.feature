#language: pt
Funcionalidade: Realizar a marcação de mensagens para acessa-las posteriormente

Como usuário
Eu quero marcar mensagens
Para poder encontra-las facilmente

Cenário: Marcar mensagem enviada por um contato
    Dado que esteja na tela de conversa de um contato
    Quando seleciono uma mensagem enviada pelo contato
    E clico na opção marcar
    Então a mensagem é marcada
    E é exibido uma estrela ao lado da data da mensagem

Cenário: Marcar mensagem de foto enviada por um contato
    Dado que esteja na tela de conversa de um contato
    Quando seleciono uma mensagem com foto enviada pelo contato
    E clico na opção marcar
    Então a mensagem é marcada
    E é exibido uma estrela ao lado da data da mensagem

Cenário: Desmarcar mensagem marcada enviada por um contato
    Dado que esteja na tela de conversa de um contato
    Quando seleciono uma mensagem já marcada anteriormente enviada pelo contato
    E clico na opção desmarcar
    Então a mensagem é desmarcada
    E não é retirada a estrela ao lado da data da mensagem

Cenário: Acessar área de Mensagens Marcadas
    Dado que esteja na tela de Ajustes
    Quando seleciono a opção Mensagens Marcadas
    Então é exibido todas as mensagens marcadas anteriormente

Cenário: Desmarcar mensagem marcada na tela de Mensagens Marcadas
    Dado que esteja na tela de Ajustes
    E seleciono a opção Mensagens Marcadas
    Quando seleciono uma mensagem na tela
    E clico na opção desmarcar
    Então a mensagem é desmarcada
    E a mesma não estará mais sendo exibida

Cenário: Clicar no ícone > para ser redirecionado
    Dado que esteja na tela Mensagens Marcadas
    Quando clico no ícone >
    Então sou direcionado para a tela de conversa

Cenário: Apagar mensagem marcada enviada por um contato
    Dado que esteja na tela Mensagens Marcadas
    Quando seleciono uma mensagem na tela
    E clico na opção Apagar
    Então a mensagem é apagada
    E a mesma não estará mais sendo exibida	

Cenário: Desmarcar mensagem marcada pela opção Editar
    Dado que esteja na tela Mensagens Marcadas
    Quando clico na opção Editar
    E seleciono uma mensagem
    E clico no icone estrela para desmarcar
    Então a mensagem é desmarcada
    E a mesma não estará mais sendo exibida