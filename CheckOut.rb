require_relative 'pricing_rules'

class CheckOut
	
	attr_reader :list_rules

	def initialize(list_rules)
		@list_rules = list_rules
		@@list_itens.clear
		@valor_total
	end

	@@list_itens = []
	
	
	def scan(item)
		
		if !item.empty? and item_esta_na_lista_de_rules? item
			@@list_itens<<item
	        return true
	    end
		
		return false
	end

	
	def total
		
		if @@list_itens.empty?
			return 0
		else
			return calcula_valor_final
		end
	end

	
	def calcula_valor_final

		@valor_total = 0
		
		for item in @@list_itens.uniq
        	 @valor_total += calcula_valor_item item,conta_quantidade_de_item_na_list_item(item)        	
        end

		@valor_total
	end

	def calcula_valor_item item,quantidade_de_item

		valor = 0
		item_rule = list_rules.detect { |rule| rule.item == item }

		if item_rule.is_special == true

			res = quantidade_de_item.divmod item_rule.item_special
			valor += res[1] * item_rule.unit_price
			valor += res[0] * item_rule.unit_special_price

		else
			valor += quantidade_de_item * item_rule.unit_price
		end
		valor
	end

	
	def item_esta_na_lista_de_rules? item
		return true if  @list_rules.count { |rule| rule.item.include? item } != 0
		
		return false
	end

	def conta_quantidade_de_item_na_list_item item
		return @@list_itens.count { |itens| itens.include? item }
	end

end






