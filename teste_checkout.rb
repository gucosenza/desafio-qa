require 'test/unit'
require_relative 'CheckOut'
require_relative 'pricing_rules'

class TestPrice < Test::Unit::TestCase
    

    def self.startup
        @@list_rules = []
        @@list_rules << Pricing_Rules.new("A",50,3,130,true)
        @@list_rules << Pricing_Rules.new("B",30,2,45,true)
        @@list_rules << Pricing_Rules.new("C",20,0,0,false)
        @@list_rules << Pricing_Rules.new("D",15,0,0,false)

        
    end
    
    
    def test_valor_vazio
        co = CheckOut.new(@@list_rules)
        assert_false(co.scan(""))
    end 

    def test_valor_invalido
        co = CheckOut.new(@@list_rules)
        assert_false(co.scan("X"))
    end

    def test_valor_valido
        co = CheckOut.new(@@list_rules)
        assert_true(co.scan("B"))
    end

    def test_pegar_valor_A
        co = CheckOut.new(@@list_rules)
        co.scan("A"); assert_equal( 50, co.total)
    end

    def test_pegar_valor_C
        co = CheckOut.new(@@list_rules)
        co.scan("C"); assert_equal( 20, co.total)
    end

    def test_pegar_valor_AA
        co = CheckOut.new(@@list_rules)
        co.scan("A");
        co.scan("A");
        assert_equal(100, co.total)
    end

    def test_pegar_valor_AB
        co = CheckOut.new(@@list_rules)
        co.scan("A");
        assert_equal(50, co.total)
        co.scan("B");
        assert_equal(80, co.total)
    end
    
end








