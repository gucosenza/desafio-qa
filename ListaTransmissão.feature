#language: pt
Funcionalidade: Realizar a criação de uma lista de transmissão

Como usuário
Eu quero criar uma lista de transmissão
Para poder enviar a mesma mensagem para todas as pessoas

Cenário: Tentar criar lista de transmissão com 1 destinatário
    Dado que esteja na tela de Conversas
    E seleciono a opção Listas de Transmissão
    Quando clico em Nova Lista
    E seleciono um destinatário
    Então a opção de Criar fica desabilitada

Cenário: Criar lista de transmissão com mais de 1 destinatário
    Dado que esteja na tela de Conversas
    E seleciono a opção Listas de Transmissão
    Quando clico em Nova Lista
    E seleciono mais de um destinatário
    Então a opção de Criar fica habilitada

Cenário: Nomear lista de transmissão existente
    Dado que esteja na tela de Listas de Transmissão
    Quando clico no icone de informações da lista
    E tento alterar o nome
    Então o nome é alterado

Cenário: Editar lista de transmissão existente
    Dado que esteja na tela de Dados da Lista de uma lista
    Quando clico na opção Editar Lista
    Então é exibido a lista de contatos para inserção ou deleção de destinatários

Cenário: Tentar apagar destinatário de lista de transmissão existente com 2 participantes
    Dado que esteja na tela de Dados da Lista de uma lista
    Quando clico na opção Editar Lista
    E removo os destinatários deixando apenas um
    Então a opção OK para finalizar a alteração fica desabilitado
