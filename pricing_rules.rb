class Pricing_Rules
	
	attr_reader :item, :unit_price, :item_special, :unit_special_price, :is_special

	def initialize(item,unit_price,item_special,unit_special_price,is_special)
		@item = item
		@unit_price = unit_price
		@item_special = item_special
		@unit_special_price = unit_special_price
		@is_special = is_special
  end

end